function paint_area_KRCallback(fig,key,areaCData,areaoldCData,tempCData,flag)
areaCData_origin = areaCData;
areaoldCData_origin = areaoldCData;
handles = getappdata(fig,'handles');
vertices = handles.areapatch.Vertices;
minxy = min(vertices);
maxxy = max(vertices);
change = 0;
switch key.Key 
    case 'delete'
        handles.image.CData(minxy(2)+1:maxxy(2),minxy(1)+1:maxxy(1),:) = 1;
        change = 1;
    case 'rightarrow'
        if maxxy(1) < handles.imagesize(1)
            areaoldCData(minxy(2)+1:maxxy(2),minxy(1)+2:maxxy(1)+1,:)=areaCData;
            handles.image.CData = areaoldCData;
            handles.areapatch.XData = [minxy(1)+1 maxxy(1)+1 maxxy(1)+1 minxy(1)+1];
            handles.areapatch.YData = [minxy(2) minxy(2) maxxy(2) maxxy(2)];
            change = 1;
        end
    
    case 'leftarrow'
        if minxy(1) > 0   
            areaoldCData(minxy(2)+1:maxxy(2),minxy(1):maxxy(1)-1,:) = areaCData;
            handles.image.CData = areaoldCData;
            handles.areapatch.XData = [minxy(1)-1 maxxy(1)-1 maxxy(1)-1 minxy(1)-1];
            handles.areapatch.YData = [minxy(2) minxy(2) maxxy(2) maxxy(2)];
            change = 1;
        end
    case 'uparrow'
        if minxy(2) > 0   
            areaoldCData(minxy(2):maxxy(2)-1,minxy(1)+1:maxxy(1),:) = areaCData;
            handles.image.CData = areaoldCData;
            handles.areapatch.XData = [minxy(1) maxxy(1) maxxy(1) minxy(1)];
            handles.areapatch.YData = [minxy(2)-1 minxy(2)-1 maxxy(2)-1 maxxy(2)-1];
            change = 1;
        end
    case 'downarrow'
        if maxxy(2) < handles.imagesize(2)
            areaoldCData(minxy(2)+2:maxxy(2)+1,minxy(1)+1:maxxy(1),:)=areaCData;
            handles.image.CData = areaoldCData;
            handles.areapatch.XData = [minxy(1) maxxy(1) maxxy(1) minxy(1)];
            handles.areapatch.YData = [minxy(2)+1 minxy(2)+1 maxxy(2)+1 maxxy(2)+1];
            change = 1;
        end
    case 'p'
        areaoldCData_origin = handles.image.CData;
        handles.image.CData(1:maxxy(2)-minxy(2),1:maxxy(1)-minxy(1),:) = areaCData;
        handles.areapatch.XData = [0 maxxy(1)-minxy(1) maxxy(1)-minxy(1) 0];
        handles.areapatch.YData = [0 0 maxxy(2)-minxy(2) maxxy(2)-minxy(2)];
        flag = 1;
        for i=0:3
            handles.oldimagedata{5-i} = handles.oldimagedata{4-i};
        end
        tempCData = areaoldCData_origin;
        handles.oldimagedata{1} = tempCData;
        handles.fig1.KeyReleaseFcn = {@paint_area_KRCallback,areaCData_origin,areaoldCData_origin,handles.image.CData,0};
        change = 1;
    case 'r'
        if strcmp(key.Character,'r')
            flag = 1;
            areaCData = fliplr(areaCData);
            areaoldCData(minxy(2)+1:maxxy(2),minxy(1)+1:maxxy(1),:) = areaCData;
            handles.image.CData = areaoldCData;
            handles.fig1.KeyReleaseFcn = {@paint_area_KRCallback,areaCData,areaoldCData_origin,handles.image.CData,0};
            change = 1;
        else
            flag = 1;
            areaCData = flipud(areaCData);
            areaoldCData(minxy(2)+1:maxxy(2),minxy(1)+1:maxxy(1),:) = areaCData;
            handles.image.CData = areaoldCData;
            handles.fig1.KeyReleaseFcn = {@paint_area_KRCallback,areaCData,areaoldCData_origin,handles.image.CData,0};
            change = 1;
        end
end
if flag == 0 && change ==1
    for i=0:3
        handles.oldimagedata{5-i} = handles.oldimagedata{4-i};
    end
    handles.oldimagedata{1} = tempCData;
    handles.fig1.KeyReleaseFcn = {@paint_area_KRCallback,areaCData_origin,areaoldCData_origin,tempCData,1};
end
guidata(handles.output,handles);
setappdata(fig,'handles',handles);
end