function paint_area_BUCallback(fig,~)
handles = getappdata(fig,'handles');
vertices = handles.areapatch.Vertices;
minxy = min(vertices);
maxxy = max(vertices);
areaCData = handles.image.CData(minxy(2)+1:maxxy(2),minxy(1)+1:maxxy(1),:);
tempCData = handles.image.CData;
tempCData(minxy(2)+1:maxxy(2),minxy(1)+1:maxxy(1),:) = 1;
areaoldCData = tempCData;
handles.fig1.WindowButtonMotionFcn = '';
handles.fig1.KeyReleaseFcn = {@paint_area_KRCallback,areaCData,areaoldCData,handles.image.CData,0};
guidata(handles.output,handles);
setappdata(fig,'handles',handles);
end