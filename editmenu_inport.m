function editmenu_inport(~,~,fig1)
handles = getappdata(fig1,'handles');
[image_name image_path] = uigetfile({'*.bmp;*.png;*.jpg;*.jpeg','Image Files(*.bmp,*.png,*.jpeg)'},'Image Select');
if image_name == 0
    return;
end
tempCData = handles.image.CData;
[im,map] = imread([image_path image_name]);
[hight,width,~] = size(im);

if width > handles.imagesize(1)
    tmpCData = ones(handles.imagesize(2),width,3);
    tmpCData(1:handles.imagesize(2),1:handles.imagesize(1),:) = handles.image.CData;
    set(handles.image,'XData',[0.5 width-0.5],'CData',tmpCData);
    handles.axes.XLim = [0 width];
    handles.imagesize(1) = width;
end
    
if hight > handles.imagesize(2)
    tmpCData = ones(hight,handles.imagesize(1),3);
    tmpCData(1:handles.imagesize(2),1:handles.imagesize(1),:) = handles.image.CData;
    set(handles.image,'YData',[0.5 hight-0.5],'CData',tmpCData);
    handles.axes.YLim = [0 hight];
    handles.imagesize(2) = hight;
end
handles.imagesize

if ~isempty(map)
    areaCData = ind2rgb(im,map);
else
    areaCData = double(im)/255;
end
whos areaCData
handles.image.CData(1:hight,1:width,:) = areaCData;
guidata(handles.output,handles);
setappdata(handles.fig1,'handles',handles);