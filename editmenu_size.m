function editmenu_size(~,~,fig1)
% SIMPLE_GUI2 Select a data set from the pop-up menu, then
% click one of the plot-type push buttons. Clicking the button
% plots the selected data in the axes.

handles = getappdata(fig1,'handles');

f = figure('Visible','off','Position',[500,500,200,150],'MenuBar','none','ToolBar','None','Name','size change','NumberTitle','off');
ok = uicontrol('Style','pushbutton','String','OK','Position',[10 10 80 20],...
                'Callback',{@okbutton_Callback});
cancel = uicontrol('Style','pushbutton','String','cancel','Position',[110 10 80 20],...
                    'Callback',{@cancelbutton_Callback});
maintext = uicontrol('Style','text','String','image size','Position',[60 120 80 20]);
widthtext = uicontrol('Style','text','String','width:','Position',[50 90 50 20]);
highttext = uicontrol('Style','text','String','hight:','Position',[50 60 50 20]);
widthedit = uicontrol('Style','edit','String',num2str(handles.imagesize(1)),'Position',[110 95 50 20],...
                        'Callback',{@widthedit_Callback});
hightedit = uicontrol('Style','edit','String',num2str(handles.imagesize(2)),'Position',[110 65 50 20],...
                        'Callback',{@hightedit_Callback});
set([f,ok,cancel,maintext,widthtext,highttext,widthedit,hightedit],'Units','normalized');


f.Visible = 'on';

    function okbutton_Callback(hObject,eventdata)
        value = str2double(widthedit.String);
        if value < handles.imagesize(1)
            set(handles.image,'XData',[0.5 value-0.5],'CData',handles.image.CData(1:handles.imagesize(2),1:value,:));
        else
            tmpCData = ones(handles.imagesize(2),value,3);
            tmpCData(1:handles.imagesize(2),1:handles.imagesize(1),:) = handles.image.CData;
            set(handles.image,'XData',[0 value],'CData',tmpCData);
        end
        handles.axes.XLim = [0 value];
        handles.imagesize(1) = value;
        
        value = str2double(hightedit.String);
        if value < handles.imagesize(2)
            set(handles.image,'YData',[0.5 value-0.5],'CData',handles.image.CData(1:value,1:handles.imagesize(1),:));
        else
            tmpCData = ones(value,handles.imagesize(1),3);
            tmpCData(1:handles.imagesize(2),1:handles.imagesize(1),:) = handles.image.CData;
        set(handles.image,'YData',[0.5 value-0.5],'CData',tmpCData);
        end
        handles.axes.YLim = [0 value];
        handles.imagesize(2) = value;
        guidata(handles.output,handles);
        setappdata(handles.fig1,'handles',handles);
        delete(f); 
    end

    function cancelbutton_Callback(hObject,eventdata)
        delete(f);
    end

    function widthedit_Callback(hObject,eventdata)
        value = round(str2double(hObject.String));
        hObject.String = num2str(value);
    end

    function hightedit_Callback(hObject,eventdata)
        value = round(str2double(hObject.String));
        hObject.String = num2str(value);
    end

end