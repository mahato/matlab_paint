function paint_BDCallback(fig,~)%,handles)
handles = getappdata(fig,'handles');

for i=0:3
    handles.oldimagedata{5-i} = handles.oldimagedata{4-i};
end
handles.oldimagedata{1} = handles.image.CData;

cp = handles.axes.CurrentPoint;
x = round(cp(1,1)+0.5);
y = round(cp(1,2)+0.5);

if x < 1 || handles.imagesize(1) < x || y < 1 || handles.imagesize(2) < y
    return;
end

handles.image.CData(y,x,1) = handles.red;
handles.image.CData(y,x,2) = handles.green;
handles.image.CData(y,x,3) = handles.blue;

handles.fig1.WindowButtonMotionFcn = @paint_BMCallback;%{@paint_BMCallback,handles};
%handles.fig1.WindowButtonUpFcn = {@paint_BUCallback,handles};
guidata(handles.output,handles);
setappdata(fig,'handles',handles);
drawnow;
end