function varargout = paint(varargin)
% PAINT MATLAB code for paint.fig
%      PAINT, by itself, creates a new PAINT or raises the existing
%      singleton*.
%
%      H = PAINT returns the handle to a new PAINT or the handle to
%      the existing singleton*.
%
%      PAINT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PAINT.M with the given input arguments.
%
%      PAINT('Property','Value',...) creates a new PAINT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before paint_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to paint_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help paint

% Last Modified by GUIDE v2.5 10-May-2016 15:10:32

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @paint_OpeningFcn, ...
                   'gui_OutputFcn',  @paint_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before paint is made visible.
function paint_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to paint (see VARARGIN)

% Choose default command line output for paint
handles.output = hObject;

% Update handles structure
handles.red = 1;
handles.green = 1;
handles.blue = 1;


C1 = ones(100,100,3);
C2 = ones(1,1,3);
%axis equal;
%handles.axes = axes('XLim',[0 100],'YLim',[0 100],'Parent',handles.fig1);
%handles.axes = handles.fig1.CurrentAxes;

handles.fig1 = figure(1);
handles.fig1.Name = 'image';
handles.fig1.NumberTitle = 'off';
handles.fig1.MenuBar = 'none';
handles.fig1.ToolBar = 'figure';
handles.filemenu = uimenu(handles.fig1,'Label','File');
handles.filemenu_savef = uimenu(handles.filemenu,'Label','save as Fig','Callback',{@filemenu_savef,handles.fig1});
handles.filemenu_savei = uimenu(handles.filemenu,'Label','save as Image','Callback',{@filemenu_savei,handles.fig1});
handles.editmenu = uimenu(handles.fig1,'Label','Edit');
handles.editmenu_size = uimenu(handles.editmenu,'Label','Size','Callback',{@editmenu_size,handles.fig1});
handles.editmenu_grid = uimenu(handles.editmenu,'Label','Grid','Callback',{@editmenu_grid,handles.fig1});
handles.editmenu_inport = uimenu(handles.editmenu,'Label','Inport','Callback',{@editmenu_inport,handles.fig1});

handles.imagesize = [100 100];
handles.image = image([0.5 99.5],[0.5 99.5],C1);
%axis square;
axis image;
handles.axes = handles.fig1.CurrentAxes;

handles.oldimagedata = cell(5,1);
%handles.oldimagedata{1} = handles.image.CData;
handles.colorimage = image(C2,'Parent',handles.axes2);
handles.fig1.WindowButtonDownFcn = @paint_BDCallback;%{@paint_BDCallback,handles};
handles.fig1.WindowButtonUpFcn = @paint_BUCallback;%{@paint_BUCallback,handles};
handles.fig1.Pointer = 'custom';
pointer_pencil = eye(16,16);
pointer_pencil(pointer_pencil==0)=NaN;
handles.pointer.pencil = {pointer_pencil,[1 1]};
pointer_fill= [0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0;
               0 0 0 0 0 0 1 2 1 0 0 0 0 0 0 0;
               0 0 0 0 0 1 2 2 2 1 0 0 0 0 0 0;
               0 0 0 0 1 2 2 2 2 2 1 0 0 0 0 0;
               0 0 0 1 2 2 2 2 2 2 2 1 0 0 0 0;
               0 0 1 1 1 1 1 1 1 1 1 1 1 0 0 0;
               0 0 1 1 1 1 1 1 1 1 1 1 1 1 0 0;
               0 0 1 0 1 1 1 1 1 1 1 1 1 1 1 0;
               0 0 1 0 0 1 1 1 1 1 1 1 1 1 0 0;
               0 0 1 0 0 0 1 1 1 1 1 1 1 0 0 0;
               0 0 1 0 0 0 0 1 1 1 1 1 0 0 0 0;
               0 0 1 0 0 0 0 0 1 1 1 0 0 0 0 0;
               0 0 1 0 0 0 0 0 0 1 0 0 0 0 0 0;
               0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0;
               0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0;
               0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
pointer_fill(pointer_fill==0)=NaN;     
handles.pointer.fill = {pointer_fill,[15 3]};
pointer_spuit= [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
                0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0;
                0 1 2 1 0 0 0 0 0 0 0 0 0 0 0 0;
                0 0 1 2 1 0 0 0 0 0 0 0 0 0 0 0;
                0 0 0 1 2 1 0 0 0 0 0 0 0 0 0 0;
                0 0 0 0 1 2 1 0 0 0 0 0 0 0 0 0;
                0 0 0 0 0 1 2 1 0 0 0 0 0 0 0 0;
                0 0 0 0 0 0 1 2 1 0 0 0 0 0 0 0;
                0 0 0 0 0 0 0 1 2 1 0 0 0 0 0 0;
                0 0 0 0 0 0 0 0 1 2 1 1 0 0 0 0;
                0 0 0 0 0 0 0 0 0 1 2 2 1 0 0 0;
                0 0 0 0 0 0 0 0 0 1 2 2 2 1 0 0;
                0 0 0 0 0 0 0 0 0 0 1 2 2 2 1 0;
                0 0 0 0 0 0 0 0 0 0 0 1 2 2 1 0;
                0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0;
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
pointer_spuit(pointer_spuit==0)=NaN;
handles.pointer.spuit = {pointer_spuit,[1 1]};
handles.fig1.PointerShapeCData = handles.pointer.pencil{1};
handles.fig1.PointerShapeHotSpot = handles.pointer.pencil{2};
handles.edit1 = findobj('Tag','edit1');
handles.edit2 = findobj('Tag','edit2');
handles.edit3 = findobj('Tag','edit3');
handles.slider1 = findobj('Tag','slider1');
handles.slider2 = findobj('Tag','slider2');
handles.slider3 = findobj('Tag','slider3');
handles.popupmenu1 = findobj('Tag','popupmenu1');

handles.fig2 = figure(2);
handles.fig2.Name = 'palette';
handles.fig2.NumberTitle = 'off';
handles.fig2.OuterPosition = [900 500 100 200];
handles.fig2.WindowButtonUpFcn = {@palette_BUCallback,handles.fig1};
C3 = ones(4,2,3);
C3(:,:,1) = [1 0;1 1;0 1;0 0.5];
C3(:,:,2) = [1 0;0 0.5;1 1;0 0];
C3(:,:,3) = [1 0;0 0;0 0;1 1];
handles.palette = image(C3);
axis image;

handles.axes2 = handles.fig2.CurrentAxes;

%minor grid
xg = [0:10:100]; 
yg = [0 100]; 
xx = reshape([xg;xg;NaN(1,length(xg))],1,length(xg)*3);
yy = repmat([yg NaN],1,length(xg));
h_minorgrid = line(xx,yy,'Color',[0.5 0.5 0.5],'LineStyle',':','Parent',handles.axes,'Visible','off'); 
h_minorgrid2 = line(yy,xx,'Color',[0.5 0.5 0.5],'LineStyle',':','Parent',handles.axes,'Visible','off');
handles.minorgrid = [h_minorgrid h_minorgrid2];

handles.fig1.CloseRequestFcn = {@paint_palette_delete,handles};
handles.fig2.CloseRequestFcn = {@paint_palette_delete,handles};
hObject.CloseRequestFcn = {@paint_palette_delete,handles};
guidata(hObject, handles);
setappdata(handles.fig1,'handles',handles);

% UIWAIT makes paint wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = paint_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
handles.red = str2double(hObject.String)/255;
handles.colorimage.CData(:,:,1) = handles.red;
handles.slider1.Value = str2double(hObject.String);
%handles.fig1.WindowButtonDownFcn = {@paint_BDCallback,handles};
%handles.fig1.WindowButtonUpFcn = {@paint_BUCallback,handles};
guidata(hObject, handles);
setappdata(handles.fig1,'handles',handles);



% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
handles.green = str2double(hObject.String)/255;
handles.colorimage.CData(:,:,2) = handles.green;
handles.slider2.Value = str2double(hObject.String);
%handles.fig1.WindowButtonDownFcn = {@paint_BDCallback,handles};
%handles.fig1.WindowButtonUpFcn = {@paint_BUCallback,handles};
guidata(hObject, handles);
setappdata(handles.fig1,'handles',handles);



% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double
handles.blue = str2double(hObject.String)/255;
handles.colorimage.CData(:,:,3) = handles.blue;
handles.slider3.Value = str2double(hObject.String);
%handles.fig1.WindowButtonDownFcn = {@paint_BDCallback,handles};
%handles.fig1.WindowButtonUpFcn = {@paint_BUCallback,handles};
guidata(hObject, handles);
setappdata(handles.fig1,'handles',handles);



% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
hObject.Value = round(hObject.Value);
handles.red = hObject.Value/255;
handles.colorimage.CData(:,:,1) = handles.red;
handles.edit1.String = num2str(hObject.Value);
%handles.fig1.WindowButtonDownFcn = {@paint_BDCallback,handles};
%handles.fig1.WindowButtonUpFcn = {@paint_BUCallback,handles};
guidata(hObject,handles);
setappdata(handles.fig1,'handles',handles);



% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
hObject.Value = round(hObject.Value);
handles.green = hObject.Value/255;
handles.colorimage.CData(:,:,2) = handles.green;
handles.edit2.String = num2str(hObject.Value);
%handles.fig1.WindowButtonDownFcn = {@paint_BDCallback,handles};
%handles.fig1.WindowButtonUpFcn = {@paint_BUCallback,handles};
guidata(hObject,handles);
setappdata(handles.fig1,'handles',handles);




% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
hObject.Value = round(hObject.Value);
handles.blue = hObject.Value/255;
handles.colorimage.CData(:,:,3) = handles.blue;
handles.edit3.String = num2str(hObject.Value);
%handles.fig1.WindowButtonDownFcn = {@paint_BDCallback,handles};
%handles.fig1.WindowButtonUpFcn = {@paint_BUCallback,handles};
guidata(hObject,handles);
setappdata(handles.fig1,'handles',handles);


% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
for i=0:3
    handles.oldimagedata{5-i} = handles.oldimagedata{4-i};
end
handles.oldimagedata{1} = handles.image.CData;
handles.image.CData = ones(handles.imagesize(2),handles.imagesize(1),3);
guidata(hObject,handles);
setappdata(handles.fig1,'handles',handles);


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.image.CData = handles.oldimagedata{1};
for i=1:4
    handles.oldimagedata{i} = handles.oldimagedata{i+1};
end
if isfield(handles,'areapatch')
    delete(handles.areapatch)
end
handles.oldimagedata{5} = handles.oldimagedata{4};
guidata(hObject,handles);
setappdata(handles.fig1,'handles',handles);


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
str = get(hObject,'String');
val = get(hObject,'Value');
if ~strcmp(str{val},'Area')
    handles.fig1.KeyReleaseFcn = '';
    if isfield(handles,'areapatch')
        delete(handles.areapatch);
    end
end

switch str{val}
    case 'Pencil'
        handles.fig1.Pointer = 'custom';
        handles.fig1.PointerShapeCData = handles.pointer.pencil{1};
        handles.fig1.PointerShapeHotSpot = handles.pointer.pencil{2};
        handles.fig1.WindowButtonDownFcn = @paint_BDCallback;
        handles.fig1.WindowButtonUpFcn = @paint_BUCallback;
    case 'Fill'
        handles.fig1.Pointer = 'custom';
        handles.fig1.PointerShapeCData = handles.pointer.fill{1};
        handles.fig1.PointerShapeHotSpot = handles.pointer.fill{2};
        handles.fig1.WindowButtonDownFcn = @paint_fill_BDCallback;
        handles.fig1.WindowButtonUpFcn = '';
    case 'Spuit'
        handles.fig1.Pointer = 'custom';
        handles.fig1.PointerShapeCData = handles.pointer.spuit{1};
        handles.fig1.PointerShapeHotSpot = handles.pointer.spuit{2};
        handles.fig1.WindowButtonDownFcn = '';
        handles.fig1.WindowButtonUpFcn = @paint_spuit_BUCallback;
    case 'Area'
        handles.fig1.Pointer = 'arrow';
        handles.fig1.WindowButtonDownFcn = @paint_area_BDCallback;
        handles.fig1.WindowButtonUpFcn = @paint_area_BUCallback;
        handles.fig1.KeyReleaseFcn = @paint_area_KRCallback;
end
guidata(hObject,handles);
setappdata(handles.fig1,'handles',handles);


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on mouse press over axes background.
function axes2_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on mouse press over axes background.
function axes3_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%{
cp = hObject.CurrentPoint;
x = round(cp(1,1));
y = round(cp(1,2));
handles.red = handles.palette.CData(y,x,1);
handles.blue = handles.palette.CData(y,x,2);
handles.green = handles.palette.CData(y,x,3);
handles.colorimage.CData(:,:,1) = handles.red;
handles.colorimage.CData(:,:,2) = handles.green;
handles.colorimage.CData(:,:,3) = handles.blue;
handles.edit1.String = num2str(round(handles.red*255));
handles.edit2.String = num2str(round(handles.green*255));
handles.edit3.String = num2str(round(handles.blue*255));
handles.slider1.Value = round(handles.red*255);
handles.slider2.Value = round(handles.green*255);
handles.slider3.Value = round(handles.blue*255);
guidata(hObject,handles);
setappdata(handles.fig1,'handles',handles);
%}


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1
edit4tag = findobj('Tag','edit4');
grid1 = handles.minorgrid(1);
grid2 = handles.minorgrid(2);
if hObject.Value == hObject.Max
    if str2double(edit4tag.String) == 0
        return
    end
    grid1.Visible = 'on';
    grid2.Visible = 'on';
else
    grid1.Visible = 'off';
    grid2.Visible = 'off';
end
guidata(hObject,handles);
setappdata(handles.fig1,'handles',handles);


function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double
gridvalue = round(str2double(hObject.String));
if gridvalue < 1
    gridvalue = 1;
elseif gridvalue > 50
    gridvalue = 50;
end
hObject.String = num2str(gridvalue);
grid1 = handles.minorgrid(1);
grid2 = handles.minorgrid(2);
xg = [0:gridvalue:100]; 
yg = [0 100]; 
xx = reshape([xg;xg;NaN(1,length(xg))],1,length(xg)*3);
yy = repmat([yg NaN],1,length(xg));
grid1.XData = xx;
grid1.YData = yy;
grid2.XData = yy;
grid2.YData = xx;
guidata(hObject,handles);
setappdata(handles.fig1,'handles',handles);

% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[image_name image_path] = uigetfile({'*.bmp;*.png;*.jpg;*.jpeg','Image Files(*.bmp,*.png,*.jpeg)'},'Image Select');
if image_name == 0
    return;
end
tempCData = handles.image.CData;
[im,map] = imread([image_path image_name]);
areaCData = ind2rgb(im,map);
[hight,width,~] = size(areaCData);
handles.image.CData(1:hight,1:width,:) = areaCData;
guidata(hObject,handles);
setappdata(handles.fig1,'handles',handles);
%handles.areapatch = patch([0 100 100 0],[0 0 100 100],'k','Parent',handles.axes,'FaceColor','none','LineStyle','--');
%handles.fig1.WindowButtonMotionFcn = '';
%handles.fig1.KeyReleaseFcn = {@paint_area_KRCallback,areaCData,areaoldCData,handles.image.CData,0};



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double
value = str2double(hObject.String);
if value < 1
    hObject.String = num2str(handles.imagesize(1));
    return;
end
value = round(value);
hObject.String = num2str(value);
if value < handles.imagesize(1)
    set(handles.image,'XData',[0.5 value-0.5],'CData',handles.image.CData(1:handles.imagesize(2),1:value,:));
else
    tmpCData = ones(handles.imagesize(2),value,3);
    tmpCData(1:handles.imagesize(2),1:handles.imagesize(1),:) = handles.image.CData;
    set(handles.image,'XData',[0 value],'CData',tmpCData);
end
handles.axes.XLim = [0 value];
handles.imagesize(1) = value;
guidata(hObject,handles);
setappdata(handles.fig1,'handles',handles);

% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double
value = str2double(hObject.String);
if value < 1
    hObject.String = num2str(handles.imagesize(2));
    return;
end
value = round(value);
hObject.String = num2str(value);
if value < handles.imagesize(2)
    set(handles.image,'YData',[0.5 value-0.5],'CData',handles.image.CData(1:value,1:handles.imagesize(1),:));
else
    tmpCData = ones(value,handles.imagesize(1),3);
    tmpCData(1:handles.imagesize(2),1:handles.imagesize(1),:) = handles.image.CData;
    set(handles.image,'YData',[0.5 value-0.5],'CData',tmpCData);
end
handles.axes.YLim = [0 value];
handles.imagesize(2) = value;
guidata(hObject,handles);
setappdata(handles.fig1,'handles',handles);


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
