function paint_fill_BDCallback(fig,~)
handles = getappdata(fig,'handles');
handles.fig1.WindowButtonDownFcn = '';

C = handles.image.CData;
cp = handles.axes.CurrentPoint;
Bx = round(cp(1,1)+0.5);
By = round(cp(1,2)+0.5);
if Bx < 1 || handles.imagesize(1) < Bx || By < 1 || handles.imagesize(2) < By
    return;
end

drawcolor = ones(1,1,3);
drawcolor(1,1,1) = handles.red;
drawcolor(1,1,2) = handles.green;
drawcolor(1,1,3) = handles.blue;
color = ones(1,1,3);
color(1,1,1) = C(By,Bx,1);
color(1,1,2) = C(By,Bx,2);
color(1,1,3) = C(By,Bx,3);
if drawcolor == color
    handles.fig1.WindowButtonDownFcn = @paint_fill_BDCallback;
    return
end
%check = -ones(1,1,3);
fill(Bx,By);
for i=0:3
    handles.oldimagedata{5-i} = handles.oldimagedata{4-i};
end
handles.oldimagedata{1} = handles.image.CData;
handles.image.CData = C;

handles.fig1.WindowButtonDownFcn = @paint_fill_BDCallback;
guidata(handles.output,handles);
setappdata(fig,'handles',handles);

    function fill(x,y)
%        handles.image.CData(y,x,:) = drawcolor(1,1,:);
        C(y,x,:) = drawcolor(1,1,:);
        if x < 100 
            if C(y,x+1,:) == color(1,1,:)
                fill(x+1,y);
            end
        end
        if x > 1
            if C(y,x-1,:) == color(1,1,:)
                fill(x-1,y);
            end
        end
        if y < 100
            if C(y+1,x,:) == color(1,1,:)
                fill(x,y+1);
            end
        end
        if y > 1
            if C(y-1,x,:) == color(1,1,:)
                fill(x,y-1);
            end
        end
    end
end
        