function paint_area_BMCallback(fig,~,Bx,By)
handles = getappdata(fig,'handles');
cp = handles.axes.CurrentPoint;
x = round(cp(1,1)+0.5);
y = round(cp(1,2)+0.5);

if x < 1 || handles.imagesize(1) < x || y < 1 || handles.imagesize(2) < y
    return;
end
%{
handles.areapatch.XData = [Bx,x-1,x-1,Bx];
handles.areapatch.YData = [By,By,y-1,y-1];
%}
if x < Bx 
    handles.areapatch.XData = [x-1,Bx,Bx,x-1];
else
    handles.areapatch.XData = [Bx-1,x,x,Bx-1];
end
if y < By
    handles.areapatch.YData = [y-1,y-1,By,By];
else
    handles.areapatch.YData = [By-1,By-1,y,y];    
end
guidata(handles.output,handles);
setappdata(fig,'handles',handles);
drawnow;