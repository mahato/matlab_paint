function paint_area_BDCallback(fig,~)
handles = getappdata(fig,'handles');
cp = handles.axes.CurrentPoint;
x = round(cp(1,1)+0.5);
y = round(cp(1,2)+0.5);
if(isfield(handles,'areapatch'))
    delete(handles.areapatch);
end

if x < 1 || handles.imagesize(1) < x || y < 1 || handles.imagesize(2) < y
    return;
end

handles.areapatch = patch([x-1 x x x-1],[y-1 y-1 y y],'k','Parent',handles.axes,'FaceColor','none','LineStyle','--');
handles.fig1.WindowButtonMotionFcn = {@paint_area_BMCallback,x,y};
guidata(handles.output,handles);
setappdata(fig,'handles',handles);
drawnow;