function paint_palette_delete(src,callbackdata,handles)
% Close request function 
% to display a question dialog box 
   selection = questdlg('Close  MATLAB_paint?',...
      'Close Request Function',...
      'Yes','No','Yes'); 
   switch selection, 
      case 'Yes',
         delete([handles.fig1 handles.fig2 handles.output]);
      case 'No'
      return 
   end
end