function paint_spuit_BUCallback(fig,~)
handles = getappdata(fig,'handles');
cp = handles.axes.CurrentPoint;
Bx = round(cp(1,1)+0.5);
By = round(cp(1,2)+0.5);

if Bx < 1 || handles.imagesize(1) < Bx || By < 1 || handles.imagesize(2) < By
    return;
end

handles.red = handles.image.CData(By,Bx,1);
handles.green = handles.image.CData(By,Bx,2);
handles.blue = handles.image.CData(By,Bx,3);
handles.colorimage.CData(:,:,1) = handles.red;
handles.colorimage.CData(:,:,2) = handles.green;
handles.colorimage.CData(:,:,3) = handles.blue;
handles.edit1.String = num2str(round(handles.red*255));
handles.edit2.String = num2str(round(handles.green*255));
handles.edit3.String = num2str(round(handles.blue*255));
handles.slider1.Value = round(handles.red*255);
handles.slider2.Value = round(handles.green*255);
handles.slider3.Value = round(handles.blue*255);
handles.popupmenu1.Value = 1;
handles.fig1.PointerShapeCData = handles.pointer.pencil{1};
handles.fig1.PointerShapeHotSpot = handles.pointer.pencil{2};
handles.fig1.WindowButtonDownFcn = @paint_BDCallback;
handles.fig1.WindowButtonUpFcn = @paint_BUCallback;
guidata(handles.output,handles);
setappdata(fig,'handles',handles);