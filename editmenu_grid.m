function editmenu_grid(~,~,fig1)
    handles = getappdata(fig1,'handles');
    grid1 = handles.minorgrid(1);
    grid2 = handles.minorgrid(2);
    if strcmp(handles.editmenu_grid.Checked,'on')
        handles.editmenu_grid.Checked = 'off';
        grid1.Visible = 'off';
        grid2.Visible = 'off';
    else
        handles.editmenu_grid.Checked = 'on';
        grid1.Visible = 'on';
        grid2.Visible = 'on';
    end
    guidata(handles.output,handles);
    setappdata(handles.fig1,'handles',handles);
end
