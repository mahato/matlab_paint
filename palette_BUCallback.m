function Palette_BUCallback(~,~,fig)
handles = getappdata(fig,'handles');
cp = handles.axes2.CurrentPoint;
x = round(cp(1,1));
y = round(cp(1,2));

if x < 1 || 2 < x
    return
end
if y < 1 || 4 < y
    return
end

handles.red = handles.palette.CData(y,x,1);
handles.green = handles.palette.CData(y,x,2);
handles.blue = handles.palette.CData(y,x,3);
handles.colorimage.CData(:,:,1) = handles.red;
handles.colorimage.CData(:,:,2) = handles.green;
handles.colorimage.CData(:,:,3) = handles.blue;
handles.edit1.String = num2str(round(handles.red*255));
handles.edit2.String = num2str(round(handles.green*255));
handles.edit3.String = num2str(round(handles.blue*255));
handles.slider1.Value = round(handles.red*255);
handles.slider2.Value = round(handles.green*255);
handles.slider3.Value = round(handles.blue*255);
guidata(handles.output,handles);
setappdata(handles.fig1,'handles',handles);
end