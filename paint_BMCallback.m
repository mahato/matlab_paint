function paint_BMCallback(fig,~)
handles = getappdata(fig,'handles');
cp = handles.axes.CurrentPoint;
x = round(cp(1,1)+0.5);
y = round(cp(1,2)+0.5);

if x < 1 || handles.imagesize(1) < x || y < 1 || handles.imagesize(2) < y
    return;
end

C = zeros(1,1,3);
C(1,1,1) = handles.red;
C(1,1,2) = handles.green;
C(1,1,3) = handles.blue;
%{
handles.image.CData(y,x,1) = handles.red;
handles.image.CData(y,x,2) = handles.green;
handles.image.CData(y,x,3) = handles.blue;
%}

handles.image.CData(y,x,:) = C;
%handles.fig1.WindowButtonDownFcn = {@paint_BDCallback,handles};
%handles.fig1.WindowButtonUpFcn = {@paint_BUCallback,handles};
guidata(handles.output,handles);
setappdata(fig,'handles',handles);
drawnow;
end